var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// schema for users

var commentSchema =  new Schema({
    
   	answer : {type:String},
	ans_by   : { type: String}
  , created_at    : { type: Date, default:Date.now }
  
  });

var quesSchema = new Schema({

body:{
		type: String,
		required:true
		
	},
time:{
		type: Date,
		default: Date.now
	},
	ques_by:{
		type: String,
		//required:true
	},
	comment: [commentSchema]
		
		
		
	
});


var Ques = module.exports = mongoose.model('Ques', quesSchema,'question');
//var Comment = module.exports = mongoose.model('Comment', commentSchema,'comment');



// Insert Question 
module.exports.getQues = function (callback,limit){
			Ques.find(callback).limit(limit);

};
module.exports.getQuesByEmail = function (email,callback){
			
			console.log(email);
			Ques.find({'ques_by':email},callback);
			//Ques.find(query,callback).limit(limit);

};
module.exports.getAnsByEmail = function (email,callback){
			
			console.log(email);
			Ques.find({'comment.ans_by':email},callback);
			//Ques.find(query,callback).limit(limit);

};

module.exports.addQues = function (ques,callback){
			console.log( ques);
			Ques.create(ques,callback);

};
module.exports.addComment = function (ques,comment,callback){
			

			
								
										
							//Ques.update({ '_id': Q._id },{$push: {'comment': cmnt}}).exec();
								Ques.where({'_id':ques}).update(
    								
        								{ $push: { 
        										"comment": {
        										
           										"answer": comment.answer,
           										"ans_by": comment.ans_by
        													}
    											}}
									,callback);
				




			
			
};

module.exports.getComment = function (ques,callback){
			

			
								Ques.findOne({'_id':ques}).populate("comment").sort({"comment.created_at" : -1}).exec(callback);


								
								
										//console.log('here' + ques);
							//Ques.update({ '_id': Q._id },{$push: {'comment': cmnt}}).exec();
								//Ques.where({'_id':ques}).update(
    								
        						//		{ $push: { 
        						//				"comment": {
        										
           						/*				"answer": comment.answer,
           										"ans_by": comment.ans_by
        													}
    											}}
									,callback);
*/
				




			
			
};
module.exports.delComment = function (id,callback){
			

			
								Ques.update({},{$pull:{comment :{'_id': new mongoose.Types.ObjectId(id)}}}, {multi:true}).exec(callback);


								
				

				




			
			
};


module.exports = Ques;
