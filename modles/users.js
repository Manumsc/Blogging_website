var mongoose = require('mongoose');

// schema for users

var userSchema = mongoose.Schema({

name:{
		type: String,
		
	},
create_date:{
		type: Date,
		default: Date.now
	},
	email:{
		type: String,
		required:true
	},
	password:{
		type: String
		
		
	},
});
var User = module.exports = mongoose.model('users', userSchema);

//get userSchema

module.exports.getUsers = function (callback,limit){
			User.findOne(callback).limit(limit);

};

module.exports.addUser = function (user,callback){
			User.create(user,callback);

};
module.exports.deleteUser = function (email,callback){
			var query = {email : email}
			User.remove(query,callback);

};
module.exports.updateUser = function (email,name,callback){
			var query = User.findOne({email : email}); 
			var id = null;
			
			query.exec(function(err,response){

				if(err){
						console.log('error in finding email');

				}
				else{

					var id = response.id;
					console.log('id :'+ id + 'name :'+ name);
					User.update({_id : id},{$set:{name:name}},callback);

				}

			});

			
			

};
module.exports = User;