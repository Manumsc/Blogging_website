var express = require('express');

var Ques=require('../modles/question');
var Topic=require('../modles/topics');
var router = express.Router();
/* GET users listing. */
router.get('/', function(req, res, next) {
		res.send("hello from questions");

});
router.get('/question', function(req, res, next) {
   Ques.getQues(function (error,ques){
      if(error){

          console.log('error');


      }else
      {

          res.json(ques);
          //res.redirect('/main_home');


      }




    });
});

router.get('/question/logout', function(req, res, next) {
   req.session.destroy();
   console.log('here');
   res.redirect('http://localhost:3000/home');
});

router.get('/questionbyemail', function(req, res, next) {
    var email=req.session.email;
   Ques.getQuesByEmail(email,function (error,ques){
      if(error){

          console.log('error');


      }else
      {

          
          res.json(ques);


      }




    });
});
router.get('/ansbyemail', function(req, res, next) {
    var email=req.session.email;
   Ques.getAnsByEmail(email,function (error,ques){
      if(error){

          console.log('error');


      }else
      {

          
          res.json(ques);


      }




    });
});
router.post('/question',function(req,res){
var ques=req.body;

Ques.addQues(ques,function(error, ques){
 if(error){

          console.log(error);


      }else
      {

          //res.json(ques);
          res.redirect('/main_home');


      }
});
});

router.post('/question/comment',function(req,res){
var comment=req.body;
var ques = req.body.doc_id;
console.log(ques);




Ques.addComment(ques,comment,function(error, ques){
 if(error){

          console.log(error);


      }else
      {

          //res.json(comment);
          res.redirect('/main_home');


      }
});

});
router.get('/question/getcomment/:_id', function(req, res, next) {
  var id=req.params._id;
  
   Ques.getComment(id,function (error,q){
      if(error){

          console.log(error);


      }else
      {
        
        //res.json(q.comment);
        res.redirect('/main_home');


      }

 next();


    });
  
});

router.get('/deleteComment/:_id', function(req, res, next) {
  var id=req.params._id;
  console.log(id);
   Ques.delComment(id,function (error,q){
      if(error){

          console.log(error);


      }else
      {
        
        //res.json(q);
        res.redirect('/main_home');


      }

 next();


    });
  
});

// get topics for search
router.get('/topics', function(req, res, next) {
   
   Topic.getTopics(function (error,topic){
      if(error){

          console.log(error);


      }else
      {

          
          res.json(topic);


      }




    });
});



module.exports = router;
