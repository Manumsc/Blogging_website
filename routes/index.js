var express = require('express');
var User = require('../modles/users');
var router = express.Router();
var path = require('path');
var LocalStrategy   = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');




var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/home');
};

module.exports = function(passport){




/* GET home page. */
router.get('/', function(req, res, next) {
  
  res.render('index.ejs');
});

router.get('/home', function(req, res, next) {
  
  res.render('home.ejs');
});
router.get('/main', function(req, res, next) {
  
  res.render('main.ejs');
});

router.get('/main_home',isAuthenticated, function(req, res, next) {
  
  console.log("new " + req.session.email);
  res.render('main_home.ejs',{ user : req.session.email });
});

// GET USERS 

router.get('/api/users', function(req, res, next) {
  User.getUsers(function (error,user){
      if(error){

          console.log('error');


      }else
      {

          res.json(user);


      }




    });
  });
router.post('/user/signup', function(req, res, next) {
  var user = req.body;
  User.addUser(user,function (error,user){
      if(error){

          console.log('error');


      }else
      {
          req.session.email = user.email;
          res.json(user);
          res.redirect('http://localhost:3000/main_home');


      }

 });
});
router.post('/user/signin', passport.authenticate('login',{

    successRedirect: '/main_home',
    failureRedirect: '/home',
    failureFlash : true  

  })
/*
  function(req, res, next) {
  var email = req.body.email;
  var password = req.body.password;

  

  var query = User.find({ 'email' : email} ,{ 'password' : password});
  query.select('email password');

  query.exec(function (error,user){
      if(error){

          console.log('error');


      }else{
      
            req.session.email = email;
           console.log(req.session.email);
          

          res.redirect('http://localhost:3000/main_home');
       


      }

 });
}
*/

);
  // add user

router.post('/api/users', function(req, res, next) {
  var user = req.body;
  User.addUser(user,function (error,user){
      if(error){

          console.log('error');


      }else
      {

          res.json(user);


      }

 });

});

router.delete('/api/users/:email', function(req, res, next) {
  var email = req.params.email;
  console.log("Hello World from controller" + email);
  User.deleteUser(email,function (error,user){
      if(error){

          console.log('error');


      }else
      {

          res.json(user);


      }

 });

});






module.exports = router;
return router;
}


