var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session_new = require('express-session');
  

var users = require('./routes/users');
var ques = require('./routes/ques');
var passport = require('passport');
var app = express();


var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/manu');
var db = mongoose.connection;
db.on('error',function(){

console.log('error while connectting to db');

});

// session

var MongoStore = require('connect-mongo')(session_new);

var conf = {
  db: {
    db: 'manu',
    host: '127.0.0.1',
    //port: 27017  // optional, default: 27017
    
   // username: 'admin', 
   // password: 'secret', 
  //  collection: 'mySessions' // 
  
  },
  secret: '076ee61d63aa10a125ea872411e433b9'
};

// setting db

db.once('open',function(){


console.log('connected to db');  

});


  // ...
  //app.use(cookieParser());

  
  //passport config
var passport = require('passport');
app.use(session_new({
    secret: conf.secret,
    maxAge: new Date(Date.now() + 3600),
    store: new MongoStore(conf.db)
  }));
app.use(passport.initialize());
app.use(passport.session());

var flash = require('connect-flash');
app.use(flash());

// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);
var routes = require('./routes/index')(passport);  





// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/ques', ques);

// catch 404 and forward to error handler
/*
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
*/
// error handlers

// development error handler
// will print stacktrace
/*
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

*/


  // important that this comes after session management
 





module.exports = app;

